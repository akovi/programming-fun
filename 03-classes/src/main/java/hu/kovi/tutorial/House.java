package hu.kovi.tutorial;

/**
 * Created by akovi on 19/04/15.
 */
public class House extends Building {

    public House() {
        super(2);
    }

    @Override
    public char charAtHeight(int row) {
        if (height == row) {
            return '^';
        }
        return super.charAtHeight(row);
    }

}
