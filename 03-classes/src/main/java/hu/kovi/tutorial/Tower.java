package hu.kovi.tutorial;

/**
 * Created by akovi on 19/04/15.
 */
public class Tower extends Building {

    public Tower(int height) {
        super(height);
    }

    @Override
    public char charAtHeight(int row) {
        if (height == row) {
            return 'l';
        }
        return super.charAtHeight(row);
    }

}
