package hu.kovi.tutorial;

import java.util.Random;

/**
 * Created by akovi on 19/04/15.
 */
public class SkyScraper extends Building {

    public SkyScraper(int height) {
        super(height);
    }

    @Override
    public char charAtHeight(int row) {
        if (height == row) {
            return '_';
        }
        return super.charAtHeight(row);
    }

}
