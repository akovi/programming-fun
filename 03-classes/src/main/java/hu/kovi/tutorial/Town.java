package hu.kovi.tutorial;

import javax.imageio.ImageIO;
import java.util.Random;

/**
 * Created by akovi on 17/04/15.
 *
 */
public class Town {

    public final Building[] buildings;

    public Town(int[] heights) {
        buildings = new SkyScraper[heights.length];
        for (int i = 0; i < heights.length; i++) {
            buildings[i] = new SkyScraper(heights[i]);
        }
    }

    public Town(Building[] buildings) {
        this.buildings = buildings;
    }

    public void printHorizontal() {
        for (Building skyScraper : buildings) {
            for (int i = 0; i < skyScraper.height; i++) {
                System.out.print(skyScraper.charAtHeight(i));
            }
            System.out.println();
        }
    }

    public Town(int length, int maxHeight) {
        this(randomHeights(length, maxHeight));
    }

    public static int[] randomHeights(int length, int maxHeight) {
        int[] slices = new int[length];
        Random r = new Random();
        for (int i = 0; i < length; i++) {
            int height = r.nextInt(maxHeight);
            slices[i] = height;
        }
        return slices;
    }

    public void printVertical(int displayHeight) {
        int row = displayHeight;
        while (row-- > 0) {
            for (Building building : buildings) {
                char character = building.charAtHeight(row);
                System.out.print(character);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Building[] buildings = new Building[] {new SkyScraper(12), new Tower(3), new House()};
        new Town(buildings).printVertical(15);
    }
}
