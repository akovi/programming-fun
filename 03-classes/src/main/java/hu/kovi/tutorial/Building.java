package hu.kovi.tutorial;

/**
 * Created by akovi on 24/04/15.
 */
public class Building {

    public final int height;

    public Building(int height) {
        this.height = height;
    }

    public char charAtHeight(int row) {
        if (height > row) {
            return '#';
        } else {
            return ' ';
        }

    }

}
