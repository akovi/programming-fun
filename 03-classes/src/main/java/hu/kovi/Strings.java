package hu.kovi;

/**
 * Created by akovi on 17/04/15.
 */
public class Strings {

    public void stringOperations() {

        String s1 = "a";
        String s2 = "b";

        // konkatenálás
        String s3 = s1 + s2;

        // minden osztálynak van toString() metódusa
        Integer i = new Integer(2);
        String s4 = i.toString();

        // String operációk
        String s5 = "abcd";
        // hossza
        s5.length();
        // karakter adott pozícióban
        s5.charAt(2);
        // egyenlőség vizsgálat
        s5.equals(s1);
        // összehasonlítás
        s5.compareTo("aaaa");
        // tartalmazás
        s5.contains("ab");
        // kezdet
        s5.startsWith("ab");
        // végződés
        s5.endsWith("cd");
        // hol van?
        s5.indexOf('d');
        // csere
        s5.replace("ab", "hello");
        // kisbetűsítés
        s5.toLowerCase();
        // nagybetűsítés
        s5.toUpperCase();
        // rész string
        s5.substring(3);
        // byte-ok
        s5.getBytes();
    }

}
