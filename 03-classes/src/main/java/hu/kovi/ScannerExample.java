package hu.kovi;

import java.util.Scanner;

/**
 * Created by akovi on 08/05/15.
 */
public class ScannerExample {

    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Írj be egy szöveget:");
        String szoveg = s.next();
        System.out.println("Beírt szöveg: " + szoveg);
    }
}
