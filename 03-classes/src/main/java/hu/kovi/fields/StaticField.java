package hu.kovi.fields;

/**
 * Created by akovi on 08/05/15.
 *
 * Osztálymezővel rendelkező osztály
 */
public class StaticField {
    /**
     * Változtatható osztályhoz tartozó mező
     */
    static int m;

    /**
     * Eggyel megnöveli az m értékét, és nem ad vissza semmit.
     *
     * A metódust meghívva az osztálymező állapota változik.
     */
    public static void inc() {
        m++;
    }

    public static void main(String[] args) {
        StaticField.m = 23;
        StaticField.inc();
    }

}
