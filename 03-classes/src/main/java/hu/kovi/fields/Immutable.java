package hu.kovi.fields;

/**
 * Created by akovi on 08/05/15.
 *
 * Ez egy nem változtatható/mutálható osztály.
 */
public class Immutable {

    /**
     * végleges objektum mező, csak inicializáláskor módosítható
     */
    final int m;

    /**
     * Konstruktor metódus.
     *
     * @param m az m érték
     */
    public Immutable(int m) {
        this.m = m;
    }

    /**
     * Egy új objektummal tér vissza, amiben az m értéke eggyel nagyobb.
     *
     * Akárhányszor hívjuk meg, a végeredmény egy új objektum,
     * az eredeti objektum változatlan marad.
     */
    public Immutable inc() {
        return new Immutable(m + 1);
    }


    public static void main(String[] args) {
        Immutable b = new Immutable(3);
        Immutable b2 = b.inc();
        Immutable b3 = b.inc();
    }
}
