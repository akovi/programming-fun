package hu.kovi.fields;

/**
 * Created by akovi on 08/05/15.
 *
 * Ez egy változtatható/mutálható osztály.
 */
public class Mutable {

    /**
     * Változtatható mező
     */
    int m;

    /**
     * Konstruktor metódus.
     *
     * @param m az m érték
     */
    public Mutable(int m) {
        this.m = m;
    }

    /**
     * Eggyel megnöveli az m értékét, és visszaadja a hívás fogadó objektumot.
     *
     * A metódust meghívva az objektum állapota változik.
     */
    public Mutable inc() {
        m++;
        return this;
    }

    public static void main(String[] args) {
        Mutable mutable = new Mutable(5);
        Mutable mutable2 = mutable.inc();
        Mutable mutable3 = mutable.inc();
    }
}
