package hu.kovi.roulette;

import java.util.ArrayList;

/**
 * Created by akovi on 30/04/15.
 */
public class RouletteWheel extends ArrayList<Integer> {

    @Override
    public Integer get(int index) {
        return super.get(index % size());
    }

    public RouletteWheel() {
        for (int i = 1; i < 38; i++) {
            add(i);
        }
    }

}
