package hu.kovi.roulette;

/**
 * Created by akovi on 30/04/15.
 */
public class RouletteMain {

    public static void main(String[] args) {

        //new Roulette().simulateBall(5.0);

        Roulette r = new Roulette();
        Integer winningNumber = r.spin(5.0, new RouletteWheel());
        System.out.println("Winning number " + winningNumber);


    }
}
