package hu.kovi.roulette;

import java.util.Random;

/**
 * Created by akovi on 30/04/15.
 */
public class Roulette {

    double friction = 0.01;

    Random r = new Random();

    public double simulateBall(double speed) {
        double distance = 0.0;
        while (speed > 0.01) {
            if (speed > 1.0) {
                speed = speed - friction;
                distance += speed;
            } else {
                distance += r.nextDouble() * 5.0;
                speed = 0.0;
            }
            System.out.println(distance + "m : " + speed + "m/s");
        }
        return distance;
    }

    public Integer spin(double speed, RouletteWheel wheel) {
        double distance = simulateBall(speed);

        return wheel.get((int)Math.round(distance));
    }
}
