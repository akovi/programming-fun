package hu.kovi;

/**
 * Created by akovi on 17/04/15.
 *
 */
public class StringGame {

    /**
     * A nagybetűt kisbetűre, a kisbetűt nagybetűre cseréli.
     * Ha nem betű a karakter, akkor visszaadja ugyanazt.
     *
     * A megoldáshoz használd a Character osztály metódusait:
     * Character.isAlphabetic(ch) : akkor true, ha a ch betű; minden más esetben false
     *      példa:
     *          Character.isAlphabetic('a') --> true
     *          Character.isAlphabetic('3') --> false
     * Character.isLowerCase(ch): akkor true, ha a ch kisbetű
     * Character.isUpperCase(ch): akkor true, ha a ch nagybetű
     * Character.toLowerCase(ch): nagybetűből kisbetűt csinál, a kisbetűt
     * Character.toUpperCase(ch): kisbetűből nagybetűt csinál
     * @param ch egy karakter
     * @return kisbetűből nagybetűt, nagybetűből kisbetűt, minden mást helyben hagy
     */
    public char switchCase(char ch) {
        char ret;
        if (Character.isAlphabetic(ch)) {
            if (Character.isLowerCase(ch)) {
                ret = Character.toUpperCase(ch);
            } else {
                ret = Character.toLowerCase(ch);
            }
        } else {
            ret = ch;
        }
        return ret;
    }

    /**
     * A string minden karakterével meghívja a switchCase metódust,
     * és a visszakapott karakterekből egy új stringet épít, ami a visszatérési érték lesz.
     *
     * A string adott indexen levő karakterét a charAt() metódussal
     * lehet lekérdezni. Pl: "abc".charAt(2) == 'c'.
     * @param str a bemeneti string
     * @return a bemeneti string kisbetűi naggyá, a nagybetűi kicsivé konvertálva.
     */
    public String switchCase(String str) {
        // menjünk végig az összes karakteren
        // változtassuk meg a nagyságát
        String ret = "";
        for (int i = 0; i < str.length(); i++) {
            ret += switchCase(str.charAt(i));
        }
        return ret;
    }



    public static void main(String[] args) {
        StringGame game = new StringGame();

        System.out.println(game.switchCase('a') + " == A");
        System.out.println(game.switchCase('B') + " == b");
        System.out.println(game.switchCase('4') + " == 4");
        System.out.println(game.switchCase('!') + " == !");
    }
}
