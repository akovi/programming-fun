package hu.kovi.hw4;

/**
 * Created by akovi on 08/05/15.
 *
 * Írj programot, ami bekér egy számot, és megállapítja, hogy prím-e.
 * A szám mindig kisebb, mint 1000000.
 *
 * Súgás: 2-től kezdve egészen a számig végezzük maradékos osztást a számon.
 * Ha nem találunk egyetlen olyan osztót sem, amellyel a maradék 0, akkor prím.
 */
public class Task1 {
}
