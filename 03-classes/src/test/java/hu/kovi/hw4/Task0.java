package hu.kovi.hw4;

import java.util.Scanner;

/**
 * Created by akovi on 08/05/15.
 *
 * Írj programot, ami akkor lép ki, ha a felhasználó a 4-es számot adja be.
 * Amíg ez nem történik meg, mind kiírja, hogy "Kérem a 4-et!"
 *
 */
public class Task0 {

    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        do {
            System.out.println("Kérem a 4-et!");
        } while (s.nextInt() != 4);
    }
}
