package hu.kovi.hw4;

/**
 * Created by akovi on 08/05/15.
 *
 * Írj programot, ami kírja egy szám prímtényezős felbontását!.
 * Például
 * Kérem a számot: 360
 * 2 2 2 3 3 5
 *
 * Súgás: 2-től indulva keressünk osztót (amivel elosztva a számot maradékként 0-t kapunk).
 * Ha találunk, írassuk ki a képernyőre, és az osztást is végezzük el a számon.
 * Az így megváltozott számmal ismételjük a műveletet mindaddig, amíg a szám 1 nem lesz.
 *
 * Az algoritmus vizuálisan: http://www.plantuml.com/plantuml/img/VP1D2i8m48NtESKG4DHI_Db7UW_f8jYKXgPHyhF9RRfquQf7oCKSZAHMnEpovitxXbTPnutpWoBhxGRAyWJKXZWEmA0LgkCsw3YgE4cG-kOT9GGzz29xvItq8ZIeo3OlCJXGS9tG1F2XzUoC5pLWE17AYYot77yrqAiDBav8Er8YT2FR11YuuKsyzpX8oIj8JlgYpMC6-rz0siluC7dd1_fDBehCdPNKmvyRbl6VjUtORFfMac_HNOLiBFy9
 @startuml
(*) --> "szám = felhasználói input"

-->"ciklus kezd"

if "szám == 1" then
-->[true] (*)
else
-->[false] "osztó = 2"
endif

-->"maradék = szám % osztó"
if "maradék == 0" then
-->[true]"kiír osztó"
else
-->[false]"osztó++"
endif

"osztó++" --> "maradék = szám % osztó"
"kiír osztó" --> "szám /= osztó"
"szám /= osztó" --> "ciklus kezd"
@enduml
 */
public class Task2 {
}
