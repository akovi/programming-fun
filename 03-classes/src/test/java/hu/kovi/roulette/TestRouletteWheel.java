package hu.kovi.roulette;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akovi on 30/04/15.
 */
public class TestRouletteWheel {

    @Test
    public void circularity() {
        RouletteWheel rouletteWheel = new RouletteWheel();

        Assert.assertEquals(2, rouletteWheel.get(1).intValue());
        Assert.assertEquals(2, rouletteWheel.get(38).intValue());
    }
}
