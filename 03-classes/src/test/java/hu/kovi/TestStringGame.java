package hu.kovi;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akovi on 24/04/15.
 */
public class TestStringGame {

    StringGame game = new StringGame();

    @Test
    public void lowerCase() {
        Assert.assertEquals('A', game.switchCase('a'));
    }

    @Test
    public void changeCase() {
        Assert.assertEquals("SZaMiTOGEP", game.switchCase("szAmItogep"));
    }
}
