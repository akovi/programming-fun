package hu.kovi;

import java.util.Arrays;

import org.junit.Test;

public class Tasks {

	@Test
	public void printArrayContent() {
		int[] k = new int[5];
		k[3] = 56;
		
		// for ciklussal írasd ki a 'k' tömb tartalmát 
		for (int i = 0; i < k.length; i++) {
			System.out.println(k[i]);
		}
	}
	
	@Test
	public void cycleAndSet() {
		
		int[] k = new int[10];
		
		// töltsd fel a 'k' tömböt 1-től növekvő számokkal
		// használj for ciklust
		
		for (int i = 0; i < k.length; i++) {
			k[i] = i;
		}
		
		// írasd ki a tömböt
		System.out.println("A tömb: " + Arrays.toString(k));
	}
	
	@Test
	public void stopOnSign() {
		
		int[] k = new int[] { 1, 2, 3, 4, 5, 6, 70, 8, 9 };

		// írasd ki a tömbnek a 8-nál nagyobb elemeit
		// használj for ciklust és a belsejében egy if elágazást
		for (int i = 0; i < k.length; i++) {
			if (k[i] > 8) {
				System.out.println(k[i]);
			}
		}
	}
	
	@Test
	public void factorial() {
		
		// n! az n szám faktoriálisát jelenti
		// n! = n * n-1 * n-2 *... 3 * 2 * 1 szorzat
		
		long n = 5;
		
		// számítsd ki n!-t egy for ciklussal és írasd ki a végeredményt
		
		long fac = 1;
		
		for (int i = 1; i <= n; i++) {
			fac = fac * i;
		}
		
		System.out.println(fac);
	}

    @Test
    public void sum() {
        long n = 12;

        long sum = 0;

        for (long i = n; i > 0; i--) {
            sum = sum + i;
        }

        System.out.println(sum);
    }

	@Test
	public void mystery() {
		for (String a = "a"; a.length() < 6; a += "a" ) {
			System.out.println(a);
		}
	}
}