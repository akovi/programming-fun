package hu.kovi;

import java.util.Arrays;

public class Main {

	/**
	 * A program kezdőpontja
	 */
	public static void main(String[] args) {
		System.out.println("Sziasztok!");
		
		Main mainObject = new Main();
		mainObject.describePrimitiveTypes();
		
		mainObject.describeArrays();
		
		mainObject.forLoop();
	}
	
	/**
	 * Primitív típusok
	 */
	public void describePrimitiveTypes() {
		System.out.println("Integer - egész számok");
		byte b = 123;
		System.out.println("byte - " + b);
		short s = 4535;
		System.out.println("short - " + s);
		int i = 345345;
		System.out.println("int - " + i);
		long l = 53453453454545323L;
		System.out.println("long - " + l);

		System.out.println("Floating point - lebegő pontos számok");
		float f = 23.5443f;
		double d = 53.76454;
		System.out.println("float " + f);
		System.out.println("double " + d);
		
		System.out.println("Boolean - boolean (igaz/hamis)");
		boolean bool = true;
		System.out.println(bool);
		
		System.out.println("Character - karakter");
		char ch = 'd';
		System.out.println(ch);
		
		System.out.println("String - karakterlánc");
		String str = "sziasztok!";
		System.out.println(str);
	}
	
	/**
	 * Tömbök
	 */
	public void describeArrays() {
		System.out.println("Arrays - tömbök");
		int[] numbers = new int[] { 0, 1, 3, 4 };
		System.out.println("int tömb " + Arrays.toString(numbers));
		
		// Értékadás
		// A tömb indexelése 0-tól kezdődik
		// A tömb negyedik értékének beállítása.
		numbers[3] = 6;
	}
	
	/**
	 *  A for ciklus
	 */
	public void forLoop() {
		int[] nums = new int[4];
		nums[0] = 1;
		nums[1] = 2;
		nums[2] = 3;
		nums[3] = 4;
		//nums[5] = 1; ArrayIndexOutOfBoundsException
		
		for (int i = 0; i < nums.length; i++) {
			System.out.println(nums[i]);
		}
	}
	
	/**
	 * Feltételes elágazás
	 */
	public void condition() {
		int szam = 4;
		// A feltétel igaz vagy hamis lehet (boolean)
		if (szam > 2) {
			// Igaz esetén az elágazás törzse hajtódik végre
			System.out.println( szam + " nagyobb, mint 2 ");
		} else {
			// Hamis esetén a 'máskülönben' ág hajtódik végre  
			System.out.println( szam + " nem nagyobb, mint 2 ");
		}
	}
}
