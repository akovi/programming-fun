package hu.kovi;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akovi on 07/04/15.
 */
public class TestMethods {

    // sut = System Under Test (ez a tesztelt példány)
    private final Methods sut = new Methods();

    @Test
    public void testNumbers() {
        long a = 12; // decimális 10
        long b = 012; // oktális 8
        long c = 0x12; // hexa 16
        long d = 0;
        //d = 0b101010; // bináris 42; csak Java 7 és későbbi esetén

        System.out.println(a == b);
        System.out.println(a == c);
        System.out.println(b == c);
        System.out.format("a=%d b=%d c=%d d=%d\n", a, b, c, d);

        System.out.format("0xA = %d", 0xa);
    }

    @Test
    public void addTwoNumbers() {
        Assert.assertEquals(3L, sut.add(1L, 2L));
    }

    @Test
    public void addThreeNumbers() {
        Assert.assertEquals(6L, sut.add(1L, 2L, 3L));
    }



    @Test
    public void addFourNumbers() {
        Assert.assertEquals(10L, sut.add(4, 4, 1, 1));
    }

    @Test
    public void isBiggerFirstBigger() {
        Assert.assertTrue(sut.isBigger(12.0, 11.0));
    }

    @Test
    public void isBiggerSecondBigger() {
        Assert.assertFalse(sut.isBigger(11.0, 12.0));
    }

    @Test
    public void isAlmostEqualIsTrueSecondBigger() {
        Assert.assertTrue(sut.isAlmostEqual(11.011, 11.012));
    }

    @Test
    public void isAlmostEqualIsTrueFirstBigger() {
        Assert.assertTrue(sut.isAlmostEqual(11.012, 11.011));
    }

    @Test
    public void isAlmostEqualIsFalse() {
        Assert.assertFalse(sut.isAlmostEqual(11.20, 11.011));
    }

}
