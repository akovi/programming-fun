package hu.kovi.hw;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akovi on 12/04/15.
 * Tesztek a Tasks02 osztályhoz
 */
public class TestTasks02 {

    // A tesztelt objektum (System Under Test)
    final Tasks02 sut = new Tasks02();

    /**
     * Csak 1-eseket tartalmaz
     */
    @Test
    public void isUniformOnlyOnes() {
        byte[] array = {1, 1, 1, 1, 1};
        Assert.assertTrue(sut.isUniform(array));
    }

    /**
     * Egyeseket és kettest is tartalmaz
     */
    @Test
    public void isUniformOnesAndTwo() {
        byte[] array = {1, 1, 1, 2, 1};
        Assert.assertFalse(sut.isUniform(array));
    }

    /**
     * Csak -1-et tartalmaz
     */
    @Test
    public void isUniformNegatives() {
        byte[] array = {-1, -1};
        Assert.assertTrue(sut.isUniform(array));
    }

    /**
     * Üres tömb
     */
    @Test
    public void isUniformEmpty() {
        byte[] array = {};
        Assert.assertTrue(sut.isUniform(array));
    }


    /**
     * Segédfüggvény az addToAll tesztek ellenőrzéséhez.
     * @param array a bemeneti tömb
     * @param value a hozzáadandó érték
     * @param expected az elvárt érték
     */
    private void checkArray(byte[] array, byte value, byte[] expected) {
        Assert.assertArrayEquals(expected, sut.addToAll(array, value));
    }

    /**
     * Üres tömb
     */
    @Test
    public void addToAllEmpty() {
        byte[] array = new byte[0]; // nulla hosszú tömb
        byte value = -123; // a hozzáadandó érték
        byte[] expected = {}; // elvárt érték

        checkArray(array, value, expected);
    }

    /**
     * Pozitív érték
     */
    @Test
    public void addToAllPositive() {
        byte[] array = new byte[] {1, 2};
        byte value = 2;
        byte[] expected = {3, 4};

        checkArray(array, value, expected);
    }

    /**
     * Negatív érték
     */
    @Test
    public void addToAllNegative() {
        byte[] array = new byte[] {1, 3};
        byte value = -2;
        byte[] expected = {-1, 1};

        checkArray(array, value, expected);
    }


    /**
     * Üres tömb másolása
     */
    @Test
    public void copyEmpty() {
        byte[] array = new byte[0];
        byte[] copied = sut.copy(array);

        Assert.assertArrayEquals(new byte[0], copied);
    }

    /**
     * Új tömböt ad vissza
     */
    @Test
    public void copyReturnsNewArray() {
        byte[] array = new byte[0];
        byte[] copied = sut.copy(array);

        // ne ugyanazt a tömböt adja vissza
        Assert.assertNotNull(copied);
        Assert.assertNotEquals(copied, array);
    }

    /**
     * Nem üres tömb
     */
    @Test
    public void copyNonempty() {
        Assert.assertArrayEquals(new byte[] {1,2,3}, sut.copy(new byte[] {1,2,3}));
    }
}
