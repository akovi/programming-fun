package hu.kovi;

/**
 * Demonstráció a for ciklus működéséhez
 */
public class HowTheForLoopWorks {

    public static void main(String[] args) {
        /*
        A lent implementált for ciklus a következő funkcionalitást valósítja meg
        for (i = 0; i < 3; i++) {
            System.out.println(i);
        }

        */
        for (int i = init(); condition(i); i = increment(i)) {
            System.out.format("body\t\ti=%d\n", i);
        }
        /*
            init                    // először a kezdeti értékadás hatódik végre
            condition	i=0         // a for elül tesztel, még a törzs előtt ellenőrzi a feltételt
            body		i=0         // utána végrehajtja a törzset
            increment	i=0+1       // végül az
            condition	i=1
            body		i=1
            increment	i=1+1
            condition	i=2
            body		i=2
            increment	i=2+1
            condition	i=3
         */
    }

    public static int init() {
        System.out.println("init");
        return 0;
    }

    public static boolean condition(int i) {
        System.out.format("condition\ti=%d\n", i);
        return i < 3;
    }

    public static int increment(int i) {
        System.out.format("increment\ti=%d+1\n", i);
        return i + 1;
    }
}
