package hu.kovi.hw;

/**
 * Házi feladatok
 */
public class Tasks02 {

    /**
     * SÚGÓ
     *
     * A feladatok elolvasása után nézd meg a TestTasks02 fájlt, amiben a tesztek szerepelnek.
     */

    /**
     * FELADAT
     *
     * Készítsd el az isUniform metódust, ami akkor ad vissza
     * true értéket, ha az array tömb csak ugyanolyan számokat tartalmaz.
     *
     * @param array a vizsgálandó tömb
     * @return true, ha az array tömb csak ugyanolyan értékeket tartalmaz
     */
    public boolean isUniform(byte[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] != array[0]) {
                return false;
            }
        }
        return true;
    }

    /**
     * FELADAT
     *
     * Keszítsd el az addToAll metódust, ami az array tömb minden
     * eleméhez hozzáadja a value értéket.
     *
     * @param array a módosítandó tömb
     * @param value a hozzáadandó érték
     * @return az array tömb
     */
    public byte[] addToAll(byte[] array, byte value) {
        for (int i = 0; i < array.length; i++) {
            array[i] += value;
        }
        return array;
    }

    /**
     * FELADAT
     *
     * Készítsd el a copy metódust, ami lemásolja az array
     * tömböt és a másolatot adja vissza.
     *
     * SÚGÁS
     *  - a tömb hossza nem változtatható
     *  - készíts egy új tömböt, aminek ugyanannyi a hossza, mint az array tömbnek
     *  - új tömb készítése: new byte[hossz]
     *
     * @param array a lemásolandó tömb
     * @return a másolat tömb
     */
    public byte[] copy(byte[] array) {
        byte[] copy = new byte[array.length];
        for (int i = 0; i < array.length; i++) {
            copy[i] = array[i];
        }
        return copy;
    }

    public byte[] copy2(byte[] array) {
        return array.clone();
    }
}
