package hu.kovi;

/**
 * Created by akovi on 07/04/15.
 */
public class Methods {

    /**
     * Összead két számot.
     * @param first első szám
     * @param second második szám
     * @return az összeg
     */
    public long add(long first, long second) {
        return first + second;
    }

    /**
     * Összead három számot.
     * @param first első szám
     * @param second második szám
     * @param third harmadik szám
     * @return az összeg
     */
    public long add(long first, long second, long third) {
        // az összeadást valósítsuk meg a már elkészített add() metódussal
        // adjuk össze az első két számot
        long subSum = add(first, second);
        // adjuk hozzá a harmadik számot
        return add(subSum, third);
    }

    /**
     FELADAT

     Készítsd el a négy számot összeadó metódust, ami az előzőleg elkészített add()
     metódusokat használja.
     */
    public long add(long first, long second, long third, long fourth) {
        long subSum = add(first, second, third);
        return add(subSum, fourth);
    }


    /**
     FELADAT

     Készítsd el az isBigger metódust, ami csak akkor tér vissza true értékkel,
     ha az 'a' szám nagyobb, mint b.

     Megjegyzés: double típusú értékeknél soha nem vizsgálunk egyenlőséget!
     */
    public boolean isBigger(double a, double b) {
        return a > b;
    }


    /**
     FELADAT

     Készítsd el az isAlmostEqual metódust, ami csak akkor tér vissza true értékkel,
     ha az 'a' szám és a 'b' szám különbsége kisebb, mint 0.01
     */
    public boolean isAlmostEqual(double a, double b) {
        double difference = a - b;
        double absDiff;
        if (difference < 0) {
            absDiff = -difference;
        } else {
            absDiff = difference;
        }
        return absDiff < 0.01;
    }
}
