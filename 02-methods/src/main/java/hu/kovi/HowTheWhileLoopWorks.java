package hu.kovi;

/**
 * Demonstráció a for ciklus működéséhez
 */
public class HowTheWhileLoopWorks {

    public static void main(String[] args) {
        String a = "a";
        while (a.length() < 10) {
            a += a;
            System.out.println(a);
        }
    }
}
