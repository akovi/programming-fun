package hu.kovi.progfun.planegame;

import java.awt.*;

public class Fence {

    private final int tileX;
    private final int tileY;
    private final TileBlock tileBlock;

    public Fence(TileSet tileSet, int tileX, int tileY) {
        this.tileX = tileX;
        this.tileY = tileY;
        tileBlock = new TileBlock(tileSet, 5, 12, 1, 2);
    }

    public int getTileWidth() {
        return tileBlock.getBlockTileWidth();
    }

    public void draw(Graphics g) {
        tileBlock.draw(g, tileX, tileY - tileBlock.getBlockTileHeight());
    }
}
