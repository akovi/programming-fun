package hu.kovi.progfun.planegame;

import java.awt.*;

public class House {

    final TileBlock[] houseParts;

    final int tileX;
    final int tileY;

    public House(HouseParts houseParts, int tileX, int tileY) {
        this.tileX = tileX;
        this.tileY = tileY;
        this.houseParts = new TileBlock[] {
                houseParts.getGroundFloor(),
                houseParts.get1stFloor(),
                houseParts.get2ndFloor(),
                houseParts.get3rdFloor(),
                houseParts.getRoof(),
        };
    }

    public int getTileWidth() {
        return houseParts[0].getBlockTileWidth();
    }

    public void draw(Graphics g) {
        int currentTileY = tileY;
        for (int i = 0; i < houseParts.length; i++) {
            TileBlock tileBlock = houseParts[i];
            currentTileY -= tileBlock.getBlockTileHeight();
            tileBlock.draw(g, tileX, currentTileY);
        }
    }

}
