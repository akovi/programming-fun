package hu.kovi.progfun.planegame;

public class HouseParts {

    final TileSet tileSet;

    public HouseParts(TileSet tileSet) {
        this.tileSet = tileSet;
    }

    public TileBlock getRoof() {
        return new TileBlock(tileSet, 0, 0, 5, 4);
    }

    public TileBlock get3rdFloor() {
        return new TileBlock(tileSet, 0, 4, 5, 2);
    }

    public TileBlock get2ndFloor() {
        return new TileBlock(tileSet, 0, 6, 5, 2);
    }

    public TileBlock get1stFloor() {
        return new TileBlock(tileSet, 0, 8, 5, 2);
    }

    public TileBlock getGroundFloor() {
        return new TileBlock(tileSet, 0, 10, 5, 2);
    }

}
