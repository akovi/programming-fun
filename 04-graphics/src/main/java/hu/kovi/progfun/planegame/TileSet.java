package hu.kovi.progfun.planegame;

import java.awt.*;

public class TileSet {

    private final int tw;
    private final int th;
    private final Image tileSet;

    public TileSet(int tw, int th, Image tileSet) {
        this.tw = tw;
        this.th = th;
        this.tileSet = tileSet;
    }


    public int getTileWidth() {
        return tw;
    }

    public int getTileHeight() {
        return th;
    }

    public void draw(Graphics g, int mx, int my, int tileSetStartX, int tileSetStartY, int tileSetXNum, int tileSetYNum) {

       g.drawImage(tileSet, mx, my, mx + tw * tileSetXNum, my + th * tileSetYNum, tileSetStartX * tw, tileSetStartY * th, (tileSetStartX + tileSetXNum) * tw,
               (tileSetStartY + tileSetYNum) * th, null);
    }
}
