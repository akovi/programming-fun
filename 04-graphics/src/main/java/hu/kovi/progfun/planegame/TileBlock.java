package hu.kovi.progfun.planegame;

import java.awt.*;

public class TileBlock {

    final TileSet tileSet;

    final int tileSetStartX;
    final int tileSetStartY;
    final int tileSetXNum;
    final int tileSetYNum;

    public TileBlock(TileSet tileSet, int tileSetStartX, int tileSetStartY, int tileSetXNum, int tileSetYNum) {
        this.tileSet = tileSet;
        this.tileSetStartX = tileSetStartX;
        this.tileSetStartY = tileSetStartY;

        this.tileSetXNum = tileSetXNum;
        this.tileSetYNum = tileSetYNum;
    }

    public int getBlockTileWidth() {
        return tileSetXNum;
    }

    public int getBlockTileHeight() {
        return tileSetYNum;
    }

    public void draw(Graphics g, int tx, int ty) {
        int mx = tx * tileSet.getTileWidth();
        int my = ty * tileSet.getTileHeight();

        tileSet.draw(g, mx, my, tileSetStartX, tileSetStartY, tileSetXNum, tileSetYNum);
    }

}
