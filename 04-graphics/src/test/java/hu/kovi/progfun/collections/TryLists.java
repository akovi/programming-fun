package hu.kovi.progfun.collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TryLists {

    @Test
    public void tryLists() {
        List<String> names = new ArrayList<String>();
        names.add("Nóri");
        names.add("Dávid");
        names.add("Márkó");
        names.add("Max");
        names.add("Beni");
        names.add("Misó");
        names.add("Andris");

        Collections.sort(names);

        System.out.println(names);
    }

    public class Item {
        int value;
        Item next;

        public Item(int value, Item next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public String toString() {
            return value + ", " + (next == null ? "" : next);
        }
    }

    @Test
    public void tryItems() {
        Item head = new Item(2, new Item(3, new Item(4, null)));

        System.out.println(head);
    }

    @Test
    public void tryItemsWithCycle() {
        Item a = new Item(2, null);
        Item b = new Item(3, null);

        a.next = b;
        b.next = a;

        Item head = a;
        for (int i = 0; ; ) {
            if (a == head) {
                i++;
                if (i == 2) {
                    break;
                }
            }
            System.out.println(head.value);
            head = head.next;
        }
    }

}
