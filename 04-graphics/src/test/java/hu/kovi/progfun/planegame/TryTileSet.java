package hu.kovi.progfun.planegame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class TryTileSet {

    public static void main(String[] args) throws IOException {

        final TileSet tileSet = new TileSet(32, 32, ImageIO.read(ClassLoader.getSystemResource("TileE-1.png")));

        final HouseParts houseParts = new HouseParts(tileSet);

        final int BOTTOM = 15;

        final House house = new House(houseParts, 0, BOTTOM);
        final Fence fence = new Fence(tileSet, 5, BOTTOM);
        final House house2 = new House(houseParts, 6, BOTTOM);
        final Fence fence2 = new Fence(tileSet, 11, BOTTOM);
        final House house3 = new House(houseParts, 12, BOTTOM);
        final Fence fence3 = new Fence(tileSet, 17, BOTTOM);

        JFrame frame = new JFrame("test frame");

        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                g.setColor(Color.gray);
                g.fillRect(0, 0, getWidth(), getHeight());

                house.draw(g);
                fence.draw(g);
                house2.draw(g);
                fence2.draw(g);
                house3.draw(g);
                fence3.draw(g);
            }
        };
        panel.setDoubleBuffered(true);
        frame.add(panel);

        frame.setPreferredSize(new Dimension(32*20, 32*20));
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
